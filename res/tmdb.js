const apikey = 'db51988feb32b0962fafec0e37bdf80d';
let d = new Date();
d.setDate( d.getDate() - 90 );
const maxDate = d.getFullYear()+'-'+d.getMonth()+'-'+d.getDate()

this.get = function(url){
    var Httpreq = new XMLHttpRequest(); // a new request
    Httpreq.open("GET",url,false);
    Httpreq.send(null);
    return Httpreq.responseText;          
}

this.search = function(searchString){
    return JSON.parse(this.get('https://api.themoviedb.org/3/search/multi?api_key='+apikey+'&query='+searchString+'&page=1'));
}

this.discover = function(genre){
    let options = ['vote_count','revenue','popularity']
    let selected = options[Math.floor(Math.random()*options.length)];
    return JSON.parse(this.get('https://api.themoviedb.org/3/discover/movie?api_key='+apikey+'&language=en-US&sort_by='+selected+'.desc&include_adult=false&include_video=false&page=1&release_date.lte='+maxDate+'&with_genres='+genre));
}

this.discoverSeries = function(genre){
    let options = ['vote_count','revenue','popularity']
    let selected = options[Math.floor(Math.random()*options.length)];
    return JSON.parse(this.get('https://api.themoviedb.org/3/discover/tv?api_key='+apikey+'&language=en-US&sort_by=vote_count.desc&page=1'));
}

this.getImdb = function(tipo, id){
    return JSON.parse(this.get('https://api.themoviedb.org/3/'+tipo+'/'+id+'/external_ids?api_key='+apikey)).imdb_id;
}

this.info = function(imdbId){
    let response =  JSON.parse(this.get('https://api.themoviedb.org/3/find/'+imdbId+'?api_key='+apikey+'&external_source=imdb_id'));
    if(response.tv_results.length>0){
        return this.detailsTv(response.tv_results[0].id);
    }
    else{
        return this.detailsMovie(response.movie_results[0].id);
    }
}

this.detailsTv = function(id){
    return JSON.parse(this.get('https://api.themoviedb.org/3/tv/'+id+'?api_key='+apikey));
}

this.detailsMovie = function(id){
    return JSON.parse(this.get('https://api.themoviedb.org/3/movie/'+id+'?api_key='+apikey));
}

this.getTbmdId = function(imdbId){
    let response =  JSON.parse(this.get('https://api.themoviedb.org/3/find/'+imdbId+'?api_key='+apikey+'&external_source=imdb_id'));
    if(response.tv_results.length>0){
        return response.tv_results[0].id;
    }
    else{
        return response.movie_results[0].id;
    }
}

this.getSeason = function(imdbId,season){
    return JSON.parse(this.get('https://api.themoviedb.org/3/tv/'+this.getTbmdId(imdbId)+'/season/'+season+'?api_key='+apikey));
}