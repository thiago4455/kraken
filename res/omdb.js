
const apikey = '20b4f32b';

this.get = function(url){
    var Httpreq = new XMLHttpRequest(); // a new request
    Httpreq.open("GET",url,false);
    Httpreq.send(null);
    return Httpreq.responseText;          
}

this.search = function(searchString){
    return JSON.parse(this.get('https://www.omdbapi.com/?apikey='+apikey+'&s="'+searchString+'"'));
}

this.info = function(imdbId){
    return JSON.parse(this.get('https://www.omdbapi.com/?apikey='+apikey+'&i='+imdbId));
}

this.getSeason = function(imdbId,seasonId){
    return JSON.parse(this.get('https://www.omdbapi.com/?apikey=20b4f32b&i='+imdbId+'&season='+seasonId));
}