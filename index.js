// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.
const $ = require('jquery');
const shared = require('./shared.js')
const omdb = require('./res/omdb.js')
const tmdb = require('./res/tmdb.js')
var searchProvider = shared.getSearchProvider();

shared.setInfo('none','none',0,0);

$('.splash').fadeOut(1000);

function discoverSeries(){
    let response = tmdb.discoverSeries()
    cont=0;
    if(response.total_results>0){
        var innerHtml = ['','',''];
            response.results.forEach(filme => {
            if(filme.poster_path!='undefined' && filme.poster_path!=null)
            {
                innerHtml[Math.floor(cont/6)]+='<img class="poster" src="https://image.tmdb.org/t/p/w500'+filme.poster_path+'" alt="tv" id="'+filme.id+'" onclick="f.select(this.id,this.alt)">'    
                if(cont%6==0 && cont!=0) innerHtml[Math.floor(cont/6)-1]+='<img class="poster" src="https://image.tmdb.org/t/p/w500'+filme.poster_path+'" alt="'+filme.media_type+'" id="'+filme.id+'" onclick="f.select(this.id,this.alt)">'    
                cont++;
            }
        });
        return innerHtml;
    }
    return '';
}

function discover(genre){
    let response = tmdb.discover(genre)
    cont=0;
    if(response.total_results>0){
        var innerHtml = ['','',''];
            response.results.forEach(filme => {
            if(filme.poster_path!='undefined' && filme.poster_path!=null)
            {
                innerHtml[Math.floor(cont/6)]+='<img class="poster" src="https://image.tmdb.org/t/p/w500'+filme.poster_path+'" alt="movie" id="'+filme.id+'" onclick="f.select(this.id,this.alt)">'    
                if(cont%6==0 && cont!=0) innerHtml[Math.floor(cont/6)-1]+='<img class="poster" src="https://image.tmdb.org/t/p/w500'+filme.poster_path+'" alt="'+filme.media_type+'" id="'+filme.id+'" onclick="f.select(this.id,this.alt)">'    
                cont++;   
            }
        });
        return innerHtml;
    }
    return '';
}

async function show(search){
    if(search!=''){
        $('#container').show();
        $('#main').hide();
        if(searchProvider=='omdb'){
            response = omdb.search(search)
            if(response.Search){
            var innerHtml = "";
            response.Search.forEach(filme => {
            if(filme.Poster!="N/A"){
                innerHtml+='<img class="poster" src="'+filme.Poster+'" alt="'+filme.imdbID+'" onclick="f.select(this.alt)">'
            }
            });
            $('#container').html(innerHtml);
            }
        }
        else{
            response = tmdb.search(search)
            if(response.total_results>0){
            var innerHtml = "";
            response.results.forEach(filme => {
            if((filme.media_type == 'movie' || filme.media_type == 'tv') && filme.poster_path!='undefined' && filme.poster_path!=null)
            {
                //var imdbID = tmdb.getImdb(filme.media_type,filme.id);
                innerHtml+='<img class="poster" src="https://image.tmdb.org/t/p/w500'+filme.poster_path+'" alt="'+filme.media_type+'" id="'+filme.id+'" onclick="f.select(this.id,this.alt)">'    
            }});
            $('#container').html(innerHtml);
            }
        }
    }    
    else{
        $('#container').hide();
        $('#main').show();
    }
}

this.select = function(imdbID,type){
    if(searchProvider=='omdb'){
        let info = omdb.info(imdbID)
        let title = info.Title.replace("'","").replace(".","");
        shared.setInfo(imdbID, title);

        if(info.Type=='series'){
            console.log("SERIE")
            window.location.href = "./series.html";
        }
        else{
            console.log("FILME")
            window.location.href = "./player.html";
        }
    }
    else{
        let id = tmdb.getImdb(type,imdbID);
        let info = tmdb.info(id);
        let title;
        if(info.last_episode_to_air){
            console.log("SERIE")
            title = info.name.replace("'","").replace(".","");
            shared.setInfo(id, title);
            window.location.href = "./series.html";
        }
        else{
            console.log("FILME")
            title = info.title.replace("'","").replace(".","");
            shared.setInfo(id, title);
            window.location.href = "./player.html";
        }
    }
}

if (location.hash) {
    setTimeout(function() {
  
      window.scrollTo(0, 0);
    }, 1);
}

let scroll = 0;

$(window).scroll(function (event) {
    scroll = $(window).scrollTop();
});



$(document).on("click", "a", function(){
    let tmpScroll = scroll;
    setTimeout(() => {
        window.scrollTo(0, tmpScroll);
    }, 0);
});


if(searchProvider!='omdb'){
    let series = discoverSeries();console.log(series)
    $('#series1').html($('#series1').html().replace('<!--replace-->',series[0]));
    $('#series2').html($('#series2').html().replace('<!--replace-->',series[1]));
    $('#series3').html($('#series3').html().replace('<!--replace-->',series[2]));

    let acao = discover(28);
    $('#acao1').html($('#acao1').html().replace('<!--replace-->',acao[0]));
    $('#acao2').html($('#acao2').html().replace('<!--replace-->',acao[1]));
    $('#acao3').html($('#acao3').html().replace('<!--replace-->',acao[2]));

    let drama = discover(18);
    $('#drama1').html($('#drama1').html().replace('<!--replace-->',drama[0]));
    $('#drama2').html($('#drama2').html().replace('<!--replace-->',drama[1]));
    $('#drama3').html($('#drama3').html().replace('<!--replace-->',drama[2]));

    let suspense = discover(53);
    $('#suspense1').html($('#suspense1').html().replace('<!--replace-->',suspense[0]));
    $('#suspense2').html($('#suspense2').html().replace('<!--replace-->',suspense[1]));
    $('#suspense3').html($('#suspense3').html().replace('<!--replace-->',suspense[2]));
}

$('#settings').on('click', () => {
    window.location.href = "./settings.html";
});

$('#search').keyup(() => {
    show(document.getElementById('search').value);
});

$('#btnSearch').on('click', () => {
    show(document.getElementById('search').value);
});