// This file is required by the settings.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.
const $ = require('jquery');
const shared = require('./shared.js')
$('#'+shared.getSearchProvider()).parent().addClass('active');
$('#'+shared.getTorrentProvider()).parent().addClass('active');

$('.searchProvider').click(function(){
    $('.searchProvider').parent().removeClass('active');
    $(this).parent().addClass('active');
    shared.setSearchProvider($(this).attr('name'));
    console.log(shared.getSearchProvider())
  });

  $('.torrentProvider').click(function(){
    $('.torrentProvider').parent().removeClass('active');
    $(this).parent().addClass('active');
    shared.setTorrentProvider($(this).attr('name'));
    console.log(shared.getTorrentProvider())
  });