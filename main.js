// Modules to control application life and create native browser window
const {app, BrowserWindow} = require('electron')
var path = require('path');

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow
const { ipcMain } = require( "electron" );

app.commandLine.appendSwitch('autoplay-policy', 'no-user-gesture-required');

global.info = {
  imdb: 'none',
  name: 'none',
  season: 0,
  episode: 0,
}

global.settings = {
  searchProvider: 'tmdb',
  torrentProvider: 'rarbg',
  appPath: app.getPath('userData'),
  tmpFolder: app.getPath('temp')
}

ipcMain.on( "debugConsole", ( event) => {
  mainWindow.webContents.openDevTools();
} );

ipcMain.on( "setTorrentProvider", ( event, torrentProvider ) => {
  global.settings.torrentProvider = torrentProvider;
} );

ipcMain.on( "setSearchProvider", ( event, searchProvider ) => {
  global.settings.searchProvider = searchProvider;
} );

ipcMain.on( "setImdb", ( event, imdb ) => {
  global.info.imdb = imdb;
} );

ipcMain.on( "setName", ( event, name ) => {
  global.info.name = name;
} );

ipcMain.on( "setSeason", ( event, season ) => {
  global.info.season = season;
} );

ipcMain.on( "setEpisode", ( event, episode ) => {
  global.info.episode = episode;
} );

ipcMain.on( "maximize", () => {
  mainWindow.maximize();
} );

ipcMain.on( "unmaximize", () => {
  mainWindow.unmaximize();
} );



function createWindow () {
  // Create the browser window.
  let {width, height} = require('electron').screen.getPrimaryDisplay().size

  mainWindow = new BrowserWindow({
    width: width,
    icon: path.join(__dirname, 'build/png/64x64.png'),
    height: height,
    backgroundColor: '#262730',
    webPreferences: {
      nodeIntegration: true,
      'plugins': true
    }
  })
  mainWindow.maximize();

  // and load the index.html of the app.
  mainWindow.loadFile('index.html')

  // Open the DevTools.
  // mainWindow.webContents.openDevTools()

  // Emitted when the window is closed.
  mainWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null
  })
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', function () {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow()
  }
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.