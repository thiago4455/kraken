const $ = require('jquery');
const shared = require('./shared.js')

const TorrentSearchApi = require('torrent-search-api');
const path = require('path');
const providerDirFullPath = path.join(__dirname, './providers/');
TorrentSearchApi.loadProviders(providerDirFullPath);
TorrentSearchApi.enableProvider('PirateBay');
TorrentSearchApi.enableProvider('KickassTorrents');
//TorrentSearchApi.enableProvider('Rarbg');
const WebTorrent = require('webtorrent');
const OS = require('opensubtitles-api');
const OpenSubtitles = new OS('MovieSelector');
const fs = require('fs');
const vtt = require('vtt-live-edit');
const rarbgApi = require('rarbg-api')
const info = shared.getInfo();
let vlc = false;
let vlcOpened = false;

var videoStarted = false;

$('.showOnPlay').hide();

$('title').html(info.name);
$('#btnTrocaCor').click(() => trocarCor());
$('#btnVoltar').click(() =>{
    window.location.href = "./index.html";
});
$('#btnLegenda').click(() => {
    adicionarLegenda($(this).val(electron.remote.dialog.showOpenDialog()[0]));
})

if(info.season!=0)
search(info.name + retEp(info.season,info.episode));
else
search(info.name)


function retEp(s, ep){
  var episode = '';
  if(ep<10){
    episode = '0'+ep;
  }
  else{
    episode = ''+ep;
  }
  var season = '';
  if(s<10){
    season = '0'+s;
  }
  else{
    season = ''+s;
  }

  var response = ' S'+season+'E'+episode;
  return response;
}

async function search(movie) {
  var torrents;
  var magnet;
  var indexTorrent = 0;
  $('#status').text('Buscando '+movie+'...');
  if(shared.getTorrentProvider()=='multi'){
    torrents = await TorrentSearchApi.search(movie, 'Video', 10);
    console.log("Search string: "+movie)
    console.log(torrents)
    $('#status').text('Procurando entre '+torrents.length+' torrents.');
    /*for (let i = 0; i < torrents.length; i++) {
      if(torrents[i].title.includes('H26') || torrents[i].title.includes('x26')){
        indexTorrent = i;
        break;
      }
    }*/
    magnet = torrents[indexTorrent].magnet;
  }
  else{
    const defaultParams = {
      category: null,
      limit: 25,
      sort: 'seeders',
      min_seeders: null,
      min_leechers: null,
      format: 'json_extended',
      ranked: null,
    }
    torrents = await rarbgApi.search(movie,defaultParams)
    console.log("Search string: "+movie)
    console.log(torrents)
  $('#status').text('Procurando entre '+torrents.length+' torrents.');
    /*for (let i = 0; i < torrents.length; i++) {
      if(torrents[i].title.includes('H26') || torrents[i].title.includes('x26')){
        indexTorrent = i;
        break;
      }
    }*/
    magnet = torrents[indexTorrent].download;
  }
  if(magnet)
  console.log(magnet);
  $('#status').text('Torrent encontrado, fazendo pré-download...');
  var client = new WebTorrent()

  client.add(magnet, function (torrent) {

    console.log(torrent.files);
    $('#status').text('Procurando arquivo de vídeo...');

    var maiorIndex = null;
    for (let i = 0; i < torrent.files.length; i++) {
      if(torrent.files[i].name.endsWith('.mkv')||torrent.files[i].name.endsWith('.mp4')||torrent.files[i].name.endsWith('.avi')){
        if(maiorIndex==null){
          maiorIndex = i;
        }
        else{
          if(torrent.files[i].length>torrent.files[maiorIndex].length){
            maiorIndex = i;
          }
        }
      }
    }
    var file = torrent.files[maiorIndex];
    console.log(file.name);
    $('#status').text('Arquivo de vídeo encontrado. Inciando transmissão...');

    if(file.name.endsWith('.avi')){
      $('#status').text('Aguardando legenda...');
      vlc = true;
    }
    else{
      $('.hideOnPlay').hide();
      $('.showOnPlay').show();
      file.appendTo('body',{autoplay: false});
      $('video').attr('id', 'video');
      let video =  document.getElementById("video");
      $('body').addClass('playing');
      $('html').addClass('playing');
      videoStarted=true;
      setTimeout(()=>{
          video.play().catch((error) => {
            if(error.name=='NotSupportedError'){
              vlc = true;
              $('.hideOnPlay').show();
              $('.showOnPlay').hide();
              $('#video').hide();
              $('body').removeClass('playing');
              $('html').removeClass('playing');
              $('#status').text('Executando no VLC...');
            }
          })
      },1)
      
  
      $('#video').click(() => {
        pause();
        $('#subtitlesMenu').hide();
      })

      video.ontimeupdate = function() {updateSlider()};
    video.controls = false;
    pause();
    }

    var download = require('download-file')
    var options = {
      directory: file._torrent.path,
      filename: file.path.substring(0, file.path.length-3)+'srt'
  }

    console.log(file._torrent.path+'/'+file.path);
    //$('#status').text(file._torrent.path+'/'+file.path+'...');
    if(info.season==0){
      if(fs.existsSync(file._torrent.path+'/'+file.path)){
        OpenSubtitles.search({
          sublanguageid: 'pb',       // Can be an array.join, 'all', or be omitted.
          filename: file.name,        // The video file name. Better if extension
                                      //   is included.

          imdbid: info.imdb,
          extensions: ['srt', 'vtt'], // Accepted extensions, defaults to 'srt'.
          path: file._torrent.path+'/'+file.path
      }).then(subtitles => {
          console.log(subtitles.pb);
          download(subtitles.pb.url, options,() => {openVlc(file._torrent.path+'/'+file.path); vlcOpened=true});
          if(!file.name.endsWith('.avi'))
          adicionarLegenda(subtitles.pb.vtt);
      });
      }
      else{
        OpenSubtitles.search({
          sublanguageid: 'pb',       // Can be an array.join, 'all', or be omitted.
          filename: file.name,        // The video file name. Better if extension
                                      //   is included.
          imdbid: info.imdb,
          extensions: ['srt', 'vtt'], // Accepted extensions, defaults to 'srt'.
      }).then(subtitles => {
          console.log(subtitles.pb);
          download(subtitles.pb.url, options,() => {openVlc(file._torrent.path+'/'+file.path); vlcOpened=true});
          if(!file.name.endsWith('.avi'))
          adicionarLegenda(subtitles.pb.vtt);
      });
      }
    }else{
      if(fs.existsSync(file._torrent.path+'/'+file.path)){
        OpenSubtitles.search({
          sublanguageid: 'pb',       // Can be an array.join, 'all', or be omitted.
          filename: file.name,        // The video file name. Better if extension
                                      //   is included.
          season: info.season,
          episode: info.episode, 
          imdbid: info.imdb,
          extensions: ['srt', 'vtt'], // Accepted extensions, defaults to 'srt'.
          path: file._torrent.path+'/'+file.path
      }).then(subtitles => {
          console.log(subtitles.pb);
          download(subtitles.pb.url, options,() => {openVlc(file._torrent.path+'/'+file.path); vlcOpened=true});
          if(!file.name.endsWith('.avi'))
          adicionarLegenda(subtitles.pb.vtt);
      });
      }
      else{
        OpenSubtitles.search({
          sublanguageid: 'pb',       // Can be an array.join, 'all', or be omitted.
          filename: file.name,        // The video file name. Better if extension
                                      //   is included.
          season: info.season,
          episode: info.episode, 
          imdbid: info.imdb,
          extensions: ['srt', 'vtt'], // Accepted extensions, defaults to 'srt'.
      }).then(subtitles => {
          console.log(subtitles.pb);
          download(subtitles.pb.url, options,() => {openVlc(file._torrent.path+'/'+file.path); vlcOpened=true});
          if(!file.name.endsWith('.avi'))
          adicionarLegenda(subtitles.pb.vtt);
      });
      }
    }
  })  

}

function openVlc(url){
  if(vlc && !vlcOpened){
    var VLC = require('vlc-simple-player')
    var player = new VLC(url.replace('file://',''))

    $('#btnVoltar').click(() =>{
      window.location.href = "./index.html";
      player.quit();
    });
  }
}

function adicionarLegenda(path){
  if(!path.includes('http')){
    fs.readFile(path, 'utf8', (err, data) => {
        if(err){
            alert("An error ocurred reading the file :" + err.message);
            return;
        }
        if(path.includes('.srt')){
        var vtt = srt2webvtt(data);

        console.log(vtt);

          fs.writeFileSync(path.replace('.srt','.vtt').replace('file://',''), vtt, 'utf-8', function(err) {
              if(err) {
                  return console.log(err);
              }
              console.log(path.replace('.srt','.vtt').replace('file://',''));
              console.log("The file was saved!");
          }); 
        }
        $('<track kind="subtitles" label="Legenda adicionada" src="'+path.replace('.srt','.vtt').replace('file://','')+'" srclang="en" default></track>').appendTo('video');
    });
  }
  else{
    $('<track kind="subtitles" label="Legendas automática" src="'+path.replace('.srt','.vtt').replace('file://','')+'" srclang="pb" default></track>').appendTo('video');
  }

  console.log(document.getElementById("video").textTracks)

  $('#listaSubs').html('');
  for (let i = 0; i < document.getElementById("video").textTracks.length; i++) {
    if(i==document.getElementById("video").textTracks.length-1)
    $('<button id="'+(i+1)+'" class="btnSub active">'+document.getElementById("video").textTracks[i].label+'</button>').appendTo('#listaSubs');
    else
    $('<button id="'+(i+1)+'" class="btnSub">'+document.getElementById("video").textTracks[i].label+'</button>').appendTo('#listaSubs');
  }


  var legendaSelecionada = 1;
  $('.btnSub').click(function(){
    if($(this).hasClass('active')){
      $('.btnSub').removeClass('active');
      legendaSelecionada=0;
    }
    else{
      $('.btnSub').removeClass('active');
      $(this).addClass('active');
      legendaSelecionada=$(this).attr('id');
    }
    
    for (let i = 0; i < document.getElementById("video").textTracks.length; i++) {
      document.getElementById("video").textTracks[i].mode='disabled';
    }
    if(legendaSelecionada!=0){
      document.getElementById("video").textTracks[legendaSelecionada-1].mode='showing';
    }
  })
  
}

$(document).keydown(function(event){
switch(event.which) {
    case 37: // left
    document.getElementById("video").currentTime -= 5;
    break;

    case 38: // up
    vtt.moveUp('video');
    break;

    case 39: // right
    document.getElementById("video").currentTime += 5;
    break;

    case 40: // down
    vtt.moveDown('video')
    break;

    case 27: //esc
    fullscreen=false;
    $('#fullscreen').html('<path d="M6 14c-.55 0-1 .45-1 1v3c0 .55.45 1 1 1h3c.55 0 1-.45 1-1s-.45-1-1-1H7v-2c0-.55-.45-1-1-1zm0-4c.55 0 1-.45 1-1V7h2c.55 0 1-.45 1-1s-.45-1-1-1H6c-.55 0-1 .45-1 1v3c0 .55.45 1 1 1zm11 7h-2c-.55 0-1 .45-1 1s.45 1 1 1h3c.55 0 1-.45 1-1v-3c0-.55-.45-1-1-1s-1 .45-1 1v2zM14 6c0 .55.45 1 1 1h2v2c0 .55.45 1 1 1s1-.45 1-1V6c0-.55-.45-1-1-1h-3c-.55 0-1 .45-1 1z"/>')
    $('#subtitlesMenu').hide();
    return;

    case 32: // space
    pause();
    $('#subtitlesMenu').hide();
    break;


    case 123:
    shared.debug();
    break;

    case 78: //n
    vtt.removeOffset('video', 0.5);
    break;

    case 77: //m
    vtt.addOffset('video', 0.5);
    break;

    default: return; // exit this handler for other keys
}
event.preventDefault();
});

$('#play').click(() => {
  pause();
})

var fullscreen = false;
$('#fullscreen').click(() => {
  if(fullscreen){
    fullscreen=false;
    closeFullscreen();
    $('#fullscreen').html('<path d="M6 14c-.55 0-1 .45-1 1v3c0 .55.45 1 1 1h3c.55 0 1-.45 1-1s-.45-1-1-1H7v-2c0-.55-.45-1-1-1zm0-4c.55 0 1-.45 1-1V7h2c.55 0 1-.45 1-1s-.45-1-1-1H6c-.55 0-1 .45-1 1v3c0 .55.45 1 1 1zm11 7h-2c-.55 0-1 .45-1 1s.45 1 1 1h3c.55 0 1-.45 1-1v-3c0-.55-.45-1-1-1s-1 .45-1 1v2zM14 6c0 .55.45 1 1 1h2v2c0 .55.45 1 1 1s1-.45 1-1V6c0-.55-.45-1-1-1h-3c-.55 0-1 .45-1 1z"/>')
  }else{
    fullscreen=true;
    openFullscreen();
    $('#fullscreen').html('<path d="M6 16h2v2c0 .55.45 1 1 1s1-.45 1-1v-3c0-.55-.45-1-1-1H6c-.55 0-1 .45-1 1s.45 1 1 1zm2-8H6c-.55 0-1 .45-1 1s.45 1 1 1h3c.55 0 1-.45 1-1V6c0-.55-.45-1-1-1s-1 .45-1 1v2zm7 11c.55 0 1-.45 1-1v-2h2c.55 0 1-.45 1-1s-.45-1-1-1h-3c-.55 0-1 .45-1 1v3c0 .55.45 1 1 1zm1-11V6c0-.55-.45-1-1-1s-1 .45-1 1v3c0 .55.45 1 1 1h3c.55 0 1-.45 1-1s-.45-1-1-1h-2z"/>')
  }
})

var elem = document.documentElement;
function openFullscreen() {
    elem.webkitRequestFullscreen();
    shared.maximize();
}

function closeFullscreen() {
    document.webkitExitFullscreen();
    shared.unmaximize();
}

function pause(){
  if(document.getElementById('video').paused) document.getElementById('video').play();
  else {document.getElementById('video').pause();
    $('.hideWithControls').show();
    document.body.style.cursor = "default";
  }
  if(document.getElementById('video').paused){
    $('#play').html('<path d="M8 6.82v10.36c0 .79.87 1.27 1.54.84l8.14-5.18c.62-.39.62-1.29 0-1.69L9.54 5.98C8.87 5.55 8 6.03 8 6.82z"/>');
  }
  else{
    if(videoStarted){
        $('.hideWithControls').show();
        document.body.style.cursor = "default";
        clearTimeout(timer);
        timer = setTimeout(function() {
          if(!video.paused){
            $('.hideWithControls').hide();
            $('#subtitlesMenu').hide();
            document.body.style.cursor = "none";
          }
        }, 2000);
    }
    $('#play').html('<path d="M8 19c1.1 0 2-.9 2-2V7c0-1.1-.9-2-2-2s-2 .9-2 2v10c0 1.1.9 2 2 2zm6-12v10c0 1.1.9 2 2 2s2-.9 2-2V7c0-1.1-.9-2-2-2s-2 .9-2 2z"/>');
  }
}

$(document).dblclick(() => {
  if(fullscreen){
    fullscreen=false;
    closeFullscreen();
    $('#fullscreen').html('<path d="M6 14c-.55 0-1 .45-1 1v3c0 .55.45 1 1 1h3c.55 0 1-.45 1-1s-.45-1-1-1H7v-2c0-.55-.45-1-1-1zm0-4c.55 0 1-.45 1-1V7h2c.55 0 1-.45 1-1s-.45-1-1-1H6c-.55 0-1 .45-1 1v3c0 .55.45 1 1 1zm11 7h-2c-.55 0-1 .45-1 1s.45 1 1 1h3c.55 0 1-.45 1-1v-3c0-.55-.45-1-1-1s-1 .45-1 1v2zM14 6c0 .55.45 1 1 1h2v2c0 .55.45 1 1 1s1-.45 1-1V6c0-.55-.45-1-1-1h-3c-.55 0-1 .45-1 1z"/>')
  }else{
    fullscreen=true;
    openFullscreen();
    $('#fullscreen').html('<path d="M6 16h2v2c0 .55.45 1 1 1s1-.45 1-1v-3c0-.55-.45-1-1-1H6c-.55 0-1 .45-1 1s.45 1 1 1zm2-8H6c-.55 0-1 .45-1 1s.45 1 1 1h3c.55 0 1-.45 1-1V6c0-.55-.45-1-1-1s-1 .45-1 1v2zm7 11c.55 0 1-.45 1-1v-2h2c.55 0 1-.45 1-1s-.45-1-1-1h-3c-.55 0-1 .45-1 1v3c0 .55.45 1 1 1zm1-11V6c0-.55-.45-1-1-1s-1 .45-1 1v3c0 .55.45 1 1 1h3c.55 0 1-.45 1-1s-.45-1-1-1h-2z"/>')
  }
});

var duration;
var currentTime;
var canChange = true;
function updateSlider(){
  duration = document.getElementById("video").duration;
  document.getElementById("slider").max = duration;
  currentTime = document.getElementById("video").currentTime;
  if(canChange){
    document.getElementById("slider").value = currentTime;
    document.getElementById("bar").style.width = ((100/duration)*currentTime)+'%';
  
    $('#time').text(secondsToHms(currentTime)+' / '+secondsToHms(duration))
  }
}

$('#slider').mouseenter(() => {
  $('#slider').addClass('hover');
  $('#progress').addClass('hover');
  $('#bar').addClass('hover');
})

$('#slider').mouseleave(() => {
  $('#slider').removeClass('hover');
  $('#progress').removeClass('hover');
  $('#bar').removeClass('hover');
})

$('#slider').on('input', () => {
  canChange = false;
  document.getElementById("bar").style.width = ((100/duration)*$('#slider').val())+'%';
  $('#time').text(secondsToHms($('#slider').val())+' / '+secondsToHms(duration))
});

$('#slider').on('change', () => {
  document.getElementById("video").currentTime = $('#slider').val();
  canChange = true;
});

$('#captions').click(() => {
  if($('#subtitlesMenu').is(":visible")){
    $('#subtitlesMenu').hide();
  }
  else{
    $('#subtitlesMenu').show();
  }
})

let timer;
$(document).mousemove(() => {
  if(videoStarted){
    if(!video.paused){
      $('.hideWithControls').show();
      document.body.style.cursor = "default";
      clearTimeout(timer);
      timer = setTimeout(function() {
        if(!video.paused){
          $('.hideWithControls').hide();
          $('#subtitlesMenu').hide();
          document.body.style.cursor = "none";
        }
      }, 3000);
    }
  }
})





function secondsToHms(sec) {
  var h = Math.floor(sec / 3600);
  var m = Math.floor(sec % 3600 / 60);
  var s = Math.floor(sec % 3600 % 60);

  var hDisplay = h > 0 ? (h>9?"":"  ") + h :"0";
  var mDisplay = m > 0 ? (m>9?"":"0") + m :"00";
  var sDisplay = s > 0 ? (s>9?"":"0") + s :"00";
  return hDisplay+':'+mDisplay+':'+sDisplay; 
}


  function srt2webvtt(data) {
    // remove dos newlines
    var srt = data.replace(/\r+/g, '');
    // trim white space start and end
    srt = srt.replace(/^\s+|\s+$/g, '');
    // get cues
    var cuelist = srt.split('\n\n');
    var result = "";
    if (cuelist.length > 0) {
      result += "WEBVTT\n\n";
      for (var i = 0; i < cuelist.length; i=i+1) {
        result += convertSrtCue(cuelist[i]);
      }
    }
    return result;
  }
  function convertSrtCue(caption) {
    // remove all html tags for security reasons
    //srt = srt.replace(/<[a-zA-Z\/][^>]*>/g, '');
    var cue = "";
    var s = caption.split(/\n/);
    // concatenate muilt-line string separated in array into one
    while (s.length > 3) {
        for (var i = 3; i < s.length; i++) {
            s[2] += "\n" + s[i]
        }
        s.splice(3, s.length - 3);
    }
    var line = 0;
    // detect identifier
    if (!s[0].match(/\d+:\d+:\d+/) && s[1].match(/\d+:\d+:\d+/)) {
      cue += s[0].match(/\w+/) + "\n";
      line += 1;
    }
    // get time strings
    if (s[line].match(/\d+:\d+:\d+/)) {
      // convert time string
      var m = s[1].match(/(\d+):(\d+):(\d+)(?:,(\d+))?\s*--?>\s*(\d+):(\d+):(\d+)(?:,(\d+))?/);
      if (m) {
        cue += m[1]+":"+m[2]+":"+m[3]+"."+m[4]+" --> "
              +m[5]+":"+m[6]+":"+m[7]+"."+m[8]+"\n";
        line += 1;
      } else {
        // Unrecognized timestring
        return "";
      }
    } else {
      // file format error or comment lines
      return "";
    }
    // get cue text
    if (s[line]) {
      cue += s[line] + "\n\n";
    }
    return cue;
  }

  var amarelo = false;
  function trocarCor(){
    if(amarelo==false){
      $('body').addClass( "amarelo" );
      amarelo = true;
    }
    else{
      $('body').removeClass( "amarelo" );
      amarelo = false;
    }

  }