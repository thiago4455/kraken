const { ipcRenderer, remote } = require( "electron" );  

this.setInfo = function(imdb, name,season,episode){
    ipcRenderer.send("setImdb",imdb);
    if(arguments.length >1) 
    ipcRenderer.send("setName",name);
    if(arguments.length >2) 
    ipcRenderer.send("setSeason",season);
    if(arguments.length >3) 
    ipcRenderer.send("setEpisode",episode);
}
this.getPath = function(){
    return remote.getGlobal( "settings" ).appPath;
}

this.getInfo = function(){
    return remote.getGlobal( "info" );
}

this.getSearchProvider = function(){
    return remote.getGlobal( "settings" ).searchProvider;
}

this.setSearchProvider = function(provider){
    ipcRenderer.send("setSearchProvider",provider);
}

this.getTorrentProvider = function(){
    return remote.getGlobal( "settings" ).torrentProvider;
}

this.setTorrentProvider = function(provider){
    ipcRenderer.send("setTorrentProvider",provider);
}

this.maximize = function(){
    ipcRenderer.send("maximize");
}

this.unmaximize = function(){
    ipcRenderer.send("maximize");
}

this.debug = function(){
    ipcRenderer.send("debugConsole");
}