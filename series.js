const $ = require('jquery');
const shared = require('./shared.js')
const omdb = require('./res/omdb.js')
const tmdb = require('./res/tmdb.js')
var seasonId = 1;

const info = shared.getInfo();
$('title').html(info.name);

this.refresh = function(newId){
    seasonId = newId;
    $('#seasons').html('');

    if(shared.getSearchProvider()=='omdb'){
        response = omdb.info(info.imdb)
        for (let i = 1; i <= response.totalSeasons; i++) {
        if(i==seasonId){
            $('<button type="button" onclick="f.refresh('+i+')" class="btn btn-outline-secondary active">Season '+i+'</button>').appendTo('#seasons')
        }
        else{
            $('<button type="button" onclick="f.refresh('+i+')" class="btn btn-outline-secondary">Season '+i+'</button>').appendTo('#seasons')
        }
        }
    }
    else{
        response = tmdb.info(info.imdb)
        for (let i = 1; i <= response.number_of_seasons; i++) {
        if(i==seasonId){
            $('<button type="button" onclick="f.refresh('+i+')" class="btn btn-outline-secondary active">Season '+i+'</button>').appendTo('#seasons')
        }
        else{
            $('<button type="button" onclick="f.refresh('+i+')" class="btn btn-outline-secondary">Season '+i+'</button>').appendTo('#seasons')
        }
        }
    }
    this.fill()
  }

this.fill =  function(){
    if(shared.getSearchProvider()=='omdb'){
        var season = omdb.getSeason(info.imdb,seasonId);

        $('#episodes').html('');
        for (let i = 0; i <= season.Episodes.length; i++) {
          $('<button type="button" onclick="f.search('+(i+1)+')" class="btn btn-outline-light episodio">'+(i+1)+'</button>').appendTo('#episodes');
        }
    }
    else{
        var season = tmdb.getSeason(info.imdb,seasonId);

        $('#episodes').html('');
        for (let i = 0; i <= season.episodes.length; i++) {
          $('<button type="button" onclick="f.search('+(i+1)+')" class="btn btn-outline-light episodio">'+(i+1)+'</button>').appendTo('#episodes');
        }
    }
}

this.search = function(ep){
    shared.setInfo(info.imdb,info.name,seasonId,ep)
    window.location.href = "./player.html";
}

this.refresh(1);